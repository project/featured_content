6.x-1.0-beta5

[#701520] by kepol: Settings to allow current node to show up in block
[#736718] by kepol: Tack on taxonomy page term to filters
[#800836] by nightlife2008 & slightly modified by kepol: Primary term support

6.x-1.0-beta4

[#726872] by kepol: Forgot the {} around blocks table name in db_query
[#748776] by kepol: Bug fix for visibility settings
[#748776] by kepol: Don't show block on edit page if visibility settings are set
[#761366] by kepol: If block shown on a non-node page, pass in path to more page
[#752590] by kepol: More instructions for visibility settings

6.x-1.0-beta3

#720590 by kepol - bug fix - use new rss data structure
#719660 by kepol - new feature - sort by closest/furthest match to current page
#713904 by kepol - new feature - Add title/header/footer to more page & title to rss page

6.x-1.0-beta2

#700044 by kepol - Allow display style of ul/ol/div for block and more page
#701516 by kepol - Add empty text to form for when there are no results
#702744 by kepol - Add CSS class based on block id number
#697576 by kepol - Rework path filtering for more flexibility
#700136 by kepol - NatureBridge reference in mouseover title text
#700042 by kepol - shuffle error with null data
fixed bug by kepol - Null array in array_intersect

Featured Content Module 6.x-dev
  * Basic Featured Content module
